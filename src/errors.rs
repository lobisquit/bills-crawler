#[derive(Debug)]
pub struct StringError(String);

impl StringError {
    pub fn new<S>(message: S) -> StringError
    where
        S: Into<String>,
    {
        StringError(message.into())
    }
}

impl std::error::Error for StringError {}

impl std::fmt::Display for StringError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<String> for StringError {
    fn from(message: String) -> Self {
        StringError(message)
    }
}
