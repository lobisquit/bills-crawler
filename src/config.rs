use crate::errors::StringError;
use log::debug;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct ImapConfig {
    /// IMAP connection configurations
    pub host: String,
    pub user: String,
    pub password: String,
    pub port: u16,
    pub tls: bool,

    /// IMAP folder and destination
    pub imap_folder: String,
}

impl std::fmt::Debug for ImapConfig {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ImapConfig")
            .field("host", &self.host)
            .field("user", &self.user)
            .field("password", &"...") // hide the password from logs
            .field("port", &self.port)
            .field("imap_folder", &self.imap_folder)
            .finish()
    }
}

#[derive(Serialize, Deserialize)]
pub struct DropboxConfig {
    /// App token provided by Dropbox
    pub token: String,

    /// Folder in the dropbox space where to put the generated folders
    /// and files
    pub dropbox_root: String,
}

impl std::fmt::Debug for DropboxConfig {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DropboxConfig")
            // hide the token from logs
            .field("token", &"...")
            .field("dropbox_root", &self.dropbox_root)
            .finish()
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GpgConfig {
    /// Mail address of the person to encrypt files for
    pub recipient: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserConfig {
    pub imap: ImapConfig,
    pub dropbox: DropboxConfig,
    pub gpg: GpgConfig,
    pub categories: Vec<String>,
}

/// Program configuration is a dictionary of many user configs
pub type ProgramConfig = HashMap<String, UserConfig>;

/// Load configuration from the program arguments
pub fn load_configuration(
    matches: clap::ArgMatches,
) -> Result<ProgramConfig, Box<dyn std::error::Error>> {
    if let Some(path) = matches.value_of("configuration path") {
        debug!("Reading from YAML file: {}", path);

        let config_str = std::fs::read_to_string(path)?;
        let config = serde_yaml::from_str(&config_str)?;

        Ok(config)
    } else if let Some(content) = matches.value_of("environment config") {
        debug!("Reading from JSON environment variable {}", content);

        let config_str = std::env::var(content)?;
        let config = serde_json::from_str(&config_str)?;

        Ok(config)
    } else {
        Err(Box::new(StringError::new(
            "Nor configuration path, nor env variables were set",
        )))
    }
}
