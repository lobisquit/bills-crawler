extern crate rustls;
extern crate rustls_connector;

mod config;
mod dropbox;
mod errors;
mod gpg;
mod handlers;
mod mail;

use clap::{App, Arg};
use log::{error, info};

fn main() {
    env_logger::init();

    let matches = App::new("Bills crawler")
        .version("1.0")
        .author("Enrico Lovisotto <git@lovisotto.it>")
        .about("Download your bills from an IMAP folder and organize them")
        .arg(
            Arg::with_name("configuration path")
                .long("path")
                .help("Load program configuration from specified yaml file [preferred]")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("environment config")
                .long("env")
                .value_name("config_env")
                .help("Load JSON program configuration from specified environment variable")
                .takes_value(true),
        )
        .get_matches();

    match config::load_configuration(matches) {
        Err(error) => error!("Configuration error: {}", error),

        Ok(program_config) => {
            info!("Configuration: {:?}", program_config);

            for (label, user_config) in program_config.iter() {
                match handlers::handle_user(user_config) {
                    Ok(_) => info!("User '{}' processed", label),
                    Err(error) => error!("User '{}' failed to process: {}", label, error),
                }
            }
        }
    }
}
