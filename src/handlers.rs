/// Process the user request given the provided configuration
use crate::config::UserConfig;
use crate::dropbox;
use crate::errors::StringError;
use crate::gpg;
use crate::mail::bill::Bill;
use crate::mail::imap::ImapSession;
use crate::mail::Mail;
use log::{error, info, warn};

/// Handle  a complete user configuration, reporting any error
pub fn handle_user(conf: &UserConfig) -> Result<(), Box<dyn std::error::Error>> {
    let mut session = ImapSession::new(&conf.imap)?;
    let unseen_mails = session.fetch_unseen()?;

    for mail in unseen_mails {
        handle_mail(conf, &mail);
    }

    session.close()?;

    Ok(())
}

/// Handle a mail account configuration, logging any error but not ending
/// the computation
fn handle_mail<'a>(conf: &'a UserConfig, mail: &'a Mail) -> Option<&'a Mail> {
    match Bill::from_mail(&conf.categories, &mail) {
        // error cases
        Err(error) => {
            error!("Mail {} - error while handling: {}", mail.get_id(), error);

            None
        }
        Ok(None) => {
            warn!("Mail {} - mail did not match any bill", mail.get_id());

            None
        }

        // correct case
        Ok(Some(bill)) => {
            info!("Bill parsed correctly: {:?}", bill.path);

            match handle_bill(conf, &bill) {
                Ok(_) => {
                    info!(
                        "Mail {} - bill handled correctly: {:?}",
                        mail.get_id(),
                        bill.path
                    );

                    Some(mail)
                }
                Err(error) => {
                    error!(
                        "Mail {} - error while handling a parsed bill: {}",
                        mail.get_id(),
                        error
                    );

                    None
                }
            }
        }
    }
}

/// Handle a bill extracted from the mail account, logging any error
/// but not ending the computation
fn handle_bill(conf: &UserConfig, bill: &Bill) -> Result<(), Box<dyn std::error::Error>> {
    // attach ".gpg" to the cleartext filename
    let clear_path: &str =
        bill.path
            .file_name()
            .map(|s| s.to_str())
            .flatten()
            .ok_or(StringError::new(format!(
                "Invalid filename, either folder or not unicode: {:?}",
                bill.path
            )))?;

    let encrypted_path = bill.path.with_file_name(format!("{}.gpg", clear_path));

    let encrypted_bill = gpg::encrypt(&conf.gpg, &bill.content, clear_path)?;
    info!("Bill encrypted: {:?}", bill.path);

    dropbox::upload(&conf.dropbox, &encrypted_path, &encrypted_bill)?;
    info!("Bill uploaded to Dropbox: {:?}", encrypted_path);

    Ok(())
}
