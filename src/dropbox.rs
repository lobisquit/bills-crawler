use crate::config;
use crate::errors::StringError;
use serde_json::json;
use std::path::Path;

pub fn upload<P: AsRef<Path>>(
    config: &config::DropboxConfig,
    dropbox_relative_path: P,
    file: &[u8],
) -> Result<ureq::Response, Box<dyn std::error::Error>> {
    // build the path of the file in the dropbox remote folder
    let dropbox_path = Path::new("/")
        .join(&config.dropbox_root)
        .join(dropbox_relative_path.as_ref());

    let dropbox_path = dropbox_path.to_str().ok_or(StringError::new(format!(
        "Provided Dropbox path cannot be written as UTF-8 string: {:?}",
        dropbox_path
    )))?;

    // build request
    let path_info = json!({ "path": dropbox_path });

    let request = ureq::post("https://content.dropboxapi.com/2/files/upload")
        .set("Authorization", &format!("Bearer {}", config.token))
        .set("Dropbox-API-Arg", &path_info.to_string())
        .set("Content-Type", "application/octet-stream");

    // perform request
    let response = request.send_bytes(file)?;

    // note that Dropbox replies with HTTP errors on conflicts (in
    // this case error 409) and other cloud filesystem issues and ureq
    // reports those failures as Err
    Ok(response)
}

#[test]
#[ignore]
fn test_upload() {
    let conf = config::DropboxConfig {
        token: "...".to_string(), // place here valid configurations when testing
        dropbox_root: "Test".to_string(),
    };

    assert_ne!(
        conf.token, "...",
        "Token should be updated to the actual value"
    );

    // upload a dummy file bytes file
    let file = &[1, 2, 3, 4, 5];
    let response = upload(&conf, "trial/test_binary.o", file);

    // check the API returns an happy response
    assert!(response.is_ok());
}
