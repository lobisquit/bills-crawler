use crate::config::ImapConfig;
use crate::mail::Mail;
use log::error;
use rustls::{ClientSession, StreamOwned};
use rustls_connector::RustlsConnector;
use std::io::{Read, Write};
use std::net::TcpStream;

/// Utility trait to be used in ImapSession as a generic Read + Write stream
trait Stream: Read + Write {}

/// Stream is implemented for the two intended transports
impl Stream for StreamOwned<ClientSession, TcpStream> {}
impl Stream for TcpStream {}

/// Wrapper for generic imap session
pub struct ImapSession(imap::Session<Box<dyn Stream>>);

impl ImapSession {
    pub fn new(conf: &ImapConfig) -> Result<ImapSession, Box<dyn std::error::Error>> {
        let stream: Box<dyn Stream> = if conf.tls {
            // provide tls stream via Rustls
            let plaintext_stream = TcpStream::connect((conf.host.as_ref(), conf.port))?;
            let tls = RustlsConnector::new_with_native_certs()?;
            let tlsstream = tls.connect(&conf.host, plaintext_stream)?;
            Box::new(tlsstream)
        } else {
            // provide plaintext tcp stream
            let plaintext_stream = TcpStream::connect((conf.host.as_ref(), conf.port))?;
            Box::new(plaintext_stream)
        };

        let client = imap::Client::new(stream);

        let mut session = client.login(&conf.user, &conf.password).map_err(|e| e.0)?;

        // open the inbox folder read-write: unread marks are
        // re-assigned upon error to the problematic mails
        session.select(&conf.imap_folder)?;

        Ok(ImapSession(session))
    }

    pub fn fetch_unseen(&mut self) -> Result<Vec<Mail>, Box<dyn std::error::Error>> {
        // fetch unseen mail ids
        let unseen_mails_ids: Vec<String> = self
            .0
            .search("UNSEEN")?
            .iter()
            .map(|n| format!("{}", n))
            .collect();

        let unseen_mails = self.0.fetch(unseen_mails_ids.join(","), "RFC822")?;

        // fetch emails and parse them
        let unseen_parsed_mails = unseen_mails_ids
            .iter()
            .zip(&unseen_mails)
            .filter_map(|(id, raw_mail)| match Mail::new(id.to_string(), raw_mail) {
                Ok(mail) => Some(mail),
                Err(error) => {
                    error!("{}", error); // report failures but filter them out
                    None
                }
            })
            .collect();

        Ok(unseen_parsed_mails)
    }

    pub fn close(mut self) -> Result<(), imap::Error> {
        self.0.close()
    }
}
