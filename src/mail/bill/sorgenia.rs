use crate::errors::StringError;
use crate::mail::bill::Bill;
use crate::mail::Mail;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::path::Path;

pub fn handle(mail: &Mail) -> Result<Option<Bill>, Box<dyn std::error::Error>> {
    lazy_static! {
        static ref SORGENIA_MONTHS: HashMap<&'static str, u8> = {
            let mut sorgenia_months: HashMap<&'static str, u8> = HashMap::new();
            sorgenia_months.insert("gennaio", 1);
            sorgenia_months.insert("febbraio", 2);
            sorgenia_months.insert("marzo", 3);
            sorgenia_months.insert("aprile", 4);
            sorgenia_months.insert("maggio", 5);
            sorgenia_months.insert("giugno", 6);
            sorgenia_months.insert("luglio", 7);
            sorgenia_months.insert("agosto", 8);
            sorgenia_months.insert("settembre", 9);
            sorgenia_months.insert("ottobre", 10);
            sorgenia_months.insert("novembre", 11);
            sorgenia_months.insert("dicembre", 12);
            sorgenia_months
        };
        static ref SORGENIA: Regex =
            Regex::new(r"Bolletta Energia Elettrica di (\w*) (\d{4})").unwrap();
    }

    if let Some(capture) = SORGENIA.captures_iter(&mail.get_subject()?).next() {
        let all_pdf_files = mail.get_pdf_parts()?;
        let first_pdf_file = all_pdf_files.iter().nth(0).ok_or(StringError::new(format!(
            "Mail has no PDF attachments: id={}",
            mail.get_id(),
        )))?;

        let month_number = SORGENIA_MONTHS[&capture[1]];
        let year = &capture[2].parse::<u16>()?;
        let path =
            Path::new("Sorgenia").join(format!("Sorgenia - {}-{:02}.pdf", year, month_number));

        return Ok(Some(Bill {
            path,
            content: first_pdf_file.to_vec(),
        }));
    }

    Ok(None)
}
