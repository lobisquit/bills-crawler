use crate::mail::Mail;
use lazy_static::lazy_static;
use log::error;
use std::collections::HashMap;
use std::path::PathBuf;

mod altotrevigiano;
mod edison_gas;
mod enel_electricity;
mod enel_gas;
mod qascom_paycheck;
mod savno;
mod sorgenia;
mod wind_tre;

#[derive(Debug)]
pub struct Bill {
    pub path: PathBuf,
    pub content: Vec<u8>,
}

type Handler =
    Box<dyn Fn(&Mail) -> Result<Option<Bill>, Box<dyn std::error::Error>> + Send + Sync + 'static>;

impl Bill {
    pub fn from_mail(
        categories: &Vec<String>,
        mail: &Mail,
    ) -> Result<Option<Bill>, Box<dyn std::error::Error>> {
        lazy_static! {
            static ref CATEGORY_HANDLERS: HashMap<&'static str, Handler> = {
                let mut cat_handler: HashMap<&'static str, Handler> = HashMap::new();

                cat_handler.insert("altotrevigiano", Box::new(altotrevigiano::handle));
                cat_handler.insert("edison_gas", Box::new(edison_gas::handle));
                cat_handler.insert("enel_electricity", Box::new(enel_electricity::handle));
                cat_handler.insert("enel_gas", Box::new(enel_gas::handle));
                cat_handler.insert("qascom_paycheck", Box::new(qascom_paycheck::handle));
                cat_handler.insert("savno", Box::new(savno::handle));
                cat_handler.insert("sorgenia", Box::new(sorgenia::handle));
                cat_handler.insert("wind_tre", Box::new(wind_tre::handle));

                cat_handler
            };
        }

        let active_handlers = categories.iter().filter_map(|label| {
            CATEGORY_HANDLERS.get(&label as &str).or_else(|| {
                error!("Invalid bill category: {}", label);
                None
            })
        });

        for handler in active_handlers {
            if let Some(bill) = handler(mail)? {
                return Ok(Some(bill));
            }
        }

        Ok(None)
    }
}
