use crate::errors::StringError;
use crate::mail::bill::Bill;
use crate::mail::Mail;
use chrono::Datelike;
use chrono::Duration;
use lazy_static::lazy_static;
use regex::Regex;
use std::path::Path;

pub fn handle(mail: &Mail) -> Result<Option<Bill>, Box<dyn std::error::Error>> {
    lazy_static! {
        static ref WINDTRE: Regex = Regex::new("e-Conto: Conto Telefonico nr ").unwrap();
    }

    if WINDTRE.is_match(&mail.get_subject()?) {
        let all_pdf_files = mail.get_pdf_parts()?;
        let first_pdf_file = all_pdf_files.iter().nth(0).ok_or(StringError::new(format!(
            "Mail has no PDF attachments: id={}",
            mail.get_id(),
        )))?;

        // move one month back, to avoid parsing the text from the pdf
        let date = mail.get_date()?;
        let bill_date = date - Duration::days(28);

        let path = Path::new("Wind Tre").join(format!(
            "Wind Tre - {}-{:02}.pdf",
            bill_date.year(),
            bill_date.month()
        ));

        return Ok(Some(Bill {
            path,
            content: first_pdf_file.to_vec(),
        }));
    }

    Ok(None)
}
