use crate::errors::StringError;
use crate::mail::bill::Bill;
use crate::mail::Mail;
use lazy_static::lazy_static;
use regex::Regex;
use std::path::Path;

pub fn handle(mail: &Mail) -> Result<Option<Bill>, Box<dyn std::error::Error>> {
    lazy_static! {
        static ref EDISON_GAS: Regex =
            Regex::new(r"Fattura Edison Energia n. \d* del (\d{2})/(\d{2})/(\d{4})").unwrap();
    }

    if let Some(capture) = EDISON_GAS.captures_iter(&mail.get_subject()?).next() {
        let all_pdf_files = mail.get_pdf_parts()?;
        let first_pdf_file = all_pdf_files.iter().nth(0).ok_or(StringError::new(format!(
            "Mail has no PDF attachments: id={}",
            mail.get_id(),
        )))?;

        let day = &capture[1]
            .parse::<u32>()
            .expect("Should not fail due to regexp");

        let month = &capture[2]
            .parse::<u32>()
            .expect("Should not fail due to regexp");

        let year = &capture[3]
            .parse::<u32>()
            .expect("Should not fail due to regexp");

        let path = Path::new("Edison Gas").join(format!(
            "Edison Gas - fattura del {}-{:02}-{:02}.pdf",
            year, month, day
        ));

        return Ok(Some(Bill {
            path,
            content: first_pdf_file.to_vec(),
        }));
    }

    Ok(None)
}
