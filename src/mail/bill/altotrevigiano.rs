use crate::errors::StringError;
use crate::mail::bill::Bill;
use crate::mail::Mail;
use chrono::{TimeZone, Utc};
use lazy_static::lazy_static;
use regex::Regex;
use std::path::Path;

pub fn handle(mail: &Mail) -> Result<Option<Bill>, Box<dyn std::error::Error>> {
    lazy_static! {
        static ref ALTOTREVIGIANO: Regex = {
            let pattern = "Alto Trevigiano Servizi: Notifica emissione fattura";
            Regex::new(pattern).unwrap()
        };
        static ref ALTOTREVIGIANO_DATE: Regex = {
            let pattern = r"<td>(\d{2})/(\d{2})/(\d{4})</td>";
            Regex::new(pattern).unwrap()
        };
        static ref ALTOTREVIGIANO_URL: Regex = {
            let pattern: &str =
                r"'(https://sgc.altotrevigianoservizi.it/webapi/visualizza-bolletta.aspx[^']*)'";
            Regex::new(pattern).unwrap()
        };
    }

    if ALTOTREVIGIANO.is_match(&mail.get_subject()?) {
        let all_html_parts = mail.get_html_parts()?;
        let content = all_html_parts
            .iter()
            .nth(0)
            .ok_or(StringError::new(format!(
                "Mail has no HTML body to parse: id={}",
                mail.get_id(),
            )))?;

        // read pdf file from the provided URL in the body, if present
        let mut pdf_file = None;

        if let Some(captures) = ALTOTREVIGIANO_URL.captures_iter(&content).next() {
            let url = &captures[1];
            pdf_file = Some(curl(url)?);
        }

        // read the bill date from another field also provided in the body
        let dates_in_table = content
            .split("\n")
            .filter_map(|line| ALTOTREVIGIANO_DATE.captures_iter(&line).next())
            .map(|captures| {
                let day: u16 = captures[1].parse().expect("Should not fail due to regexp");
                let month: u16 = captures[2].parse().expect("Should not fail due to regexp");
                let year: u16 = captures[3].parse().expect("Should not fail due to regexp");

                Utc.ymd(year.into(), month.into(), day.into())
            });

        // return if both of them are found
        if let Some(emission_date) = dates_in_table.min() {
            if let Some(pdf_file) = pdf_file {
                let path = Path::new("AltoTrevigiano").join(format!(
                    "AltoTrevigiano - fattura del {}.pdf",
                    emission_date.format("%Y-%m-%d")
                ));

                return Ok(Some(Bill {
                    path,
                    content: pdf_file,
                }));
            }
        }
    }

    Ok(None)
}

fn curl(url: &str) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
    use std::process::{Command, Stdio};

    // setup process and intercept output
    let process = Command::new("curl")
        .args(&[url, "--output", "-"])
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;

    // read output
    let output = process.wait_with_output()?;

    if output.status.success() {
        // collect the output in an owned vector of bytes to return
        let output_vector = output.stdout.iter().map(|&x| x).collect();
        Ok(output_vector)
    } else {
        let output_buf = std::str::from_utf8(&output.stderr)?;
        Err(Box::new(StringError::new(output_buf)))
    }
}
