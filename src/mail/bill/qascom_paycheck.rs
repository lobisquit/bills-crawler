use lazy_static::lazy_static;
use regex::Regex;
use std::path::Path;

use crate::errors::StringError;
use crate::mail::bill::Bill;
use crate::mail::Mail;

pub fn handle(mail: &Mail) -> Result<Option<Bill>, Box<dyn std::error::Error>> {
    lazy_static! {
        static ref PAYCHECK: Regex = Regex::new(r"Cedolini-(\d{4}-\d{2}).pdf").unwrap();
    }

    if let Some(capture) = PAYCHECK.captures_iter(&mail.get_subject()?).next() {
        let all_pdf_files = mail.get_pdf_parts()?;
        let first_pdf_file = all_pdf_files.iter().nth(0).ok_or(StringError::new(format!(
            "Mail has no PDF attachments: id={}",
            mail.get_id(),
        )))?;

        let date_str = &capture[1];
        let path = Path::new("Buste paga").join(format!("{} cedolino Qascom.pdf", date_str));

        return Ok(Some(Bill {
            path,
            content: first_pdf_file.to_vec(),
        }));
    }

    Ok(None)
}
