use lazy_static::lazy_static;
use regex::Regex;
use std::path::Path;

use crate::errors::StringError;
use crate::mail::bill::Bill;
use crate::mail::Mail;

pub fn handle(mail: &Mail) -> Result<Option<Bill>, Box<dyn std::error::Error>> {
    lazy_static! {
        static ref ENEL_ELECTRICITY: Regex =
            Regex::new(r"EMISSIONE BOLLETTAWEB DI ENERGIA ELETTRICA DI (\d{4}-\d{2}-\d{2})")
                .unwrap();
    }

    if let Some(capture) = ENEL_ELECTRICITY.captures_iter(&mail.get_subject()?).next() {
        let all_pdf_files = mail.get_pdf_parts()?;
        let first_pdf_file = all_pdf_files.iter().nth(0).ok_or(StringError::new(format!(
            "Mail has no PDF attachments: id={}",
            mail.get_id(),
        )))?;

        let date_str = &capture[1];
        let path = Path::new("Enel Corrente")
            .join(format!("Enel Corrente - fattura del {}.pdf", date_str));

        return Ok(Some(Bill {
            path,
            content: first_pdf_file.to_vec(),
        }));
    }

    Ok(None)
}
