use crate::errors::StringError;
use ::imap::types::Fetch;
use chrono::{DateTime, FixedOffset};
use log::warn;
use mailparse;
use mailparse::{MailHeaderMap, ParsedMail};

pub mod bill;
pub mod imap;

pub struct Mail {
    id: String,
    body: Vec<u8>,
}

impl Mail {
    pub fn new(id: String, raw_mail: &Fetch) -> Result<Mail, StringError> {
        let body: Vec<u8> = raw_mail
            .body()
            .ok_or(format!("Message did not have a body"))?
            .iter()
            .map(|&x| x)
            .collect();

        Ok(Mail { id, body })
    }

    pub fn get_id(&self) -> &String {
        &self.id
    }

    pub fn get_subject(&self) -> Result<String, Box<dyn std::error::Error>> {
        let (headers, _) = mailparse::parse_headers(&self.body)?;

        let subject = headers
            .get_first_value("Subject")
            .ok_or(StringError::new(format!(
                "Mail {} does not have a subject",
                self.get_id()
            )))?;

        Ok(subject)
    }

    pub fn get_date(&self) -> Result<DateTime<FixedOffset>, Box<dyn std::error::Error>> {
        let (headers, _) = mailparse::parse_headers(&self.body)?;
        let date_str = headers
            .get_first_value("Date")
            .ok_or("Could not fetch date")?;

        let date = DateTime::parse_from_rfc2822(&date_str)?;

        Ok(date)
    }

    pub fn get_html_parts(&self) -> Result<Vec<String>, Box<dyn std::error::Error>> {
        let parsed_mail = mailparse::parse_mail(&self.body)?;

        let html_parts = get_all_subparts(&parsed_mail)
            .filter(|subpart| subpart.ctype.mimetype == "text/html")
            .filter_map(|subpart| match subpart.get_body() {
                Ok(text) => Some(text),
                Err(error) => {
                    warn!(
                        "Could not parse an HTML subpart on id={}: {}",
                        self.get_id(),
                        error
                    );
                    None
                }
            });

        Ok(html_parts.collect())
    }

    pub fn get_pdf_parts(&self) -> Result<Vec<Vec<u8>>, Box<dyn std::error::Error>> {
        let parsed_mail = mailparse::parse_mail(&self.body)?;

        let attached_pdf_files = get_all_subparts(&parsed_mail)
            .filter(|subpart| {
                subpart.ctype.mimetype == "application/pdf"
                    || subpart.ctype.mimetype == "application/octet-stream"
            })
            .filter_map(|subpart| match subpart.get_body_raw() {
                Ok(file) => Some(file),
                Err(error) => {
                    warn!(
                        "Could not parse the PDF attachment on id={}: {}",
                        self.get_id(),
                        error
                    );
                    None
                }
            });

        Ok(attached_pdf_files.collect())
    }
}

/// Retrieve all subparts of a mail in a linear iterator
fn get_all_subparts<'a>(mail: &'a ParsedMail) -> Box<dyn Iterator<Item = &'a ParsedMail<'a>> + 'a> {
    let current_level = &mail.subparts;

    let nephews = current_level
        .iter()
        .map(|child| get_all_subparts(child))
        .flatten();

    Box::new(current_level.iter().chain(nephews))
}
