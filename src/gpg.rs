use crate::config::GpgConfig;
use crate::errors::StringError;
use std::io::Write;
use std::process::{Command, Stdio};

pub fn encrypt(
    conf: &GpgConfig,
    file: &[u8],
    filename: &str,
) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
    // setup process and intercept all inputs and outputs
    let mut process = Command::new("gpg")
        .args(&[
            "--encrypt",
            "--recipient",
            &conf.recipient,
            "--output",
            "-",
            "--set-filename",
            filename,
        ])
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;

    let mut stdin = process.stdin.take().unwrap();

    let file_vec: Vec<u8> = file.to_vec();
    std::thread::spawn(move || {
        stdin.write_all(&file_vec).unwrap();
    });

    // read output
    let output = process.wait_with_output()?;

    if output.status.success() {
        // collect the output in an owned vector of bytes to return
        let output_vector = output.stdout.iter().map(|&x| x).collect();
        Ok(output_vector)
    } else {
        let output_buf = std::str::from_utf8(&output.stderr)?;
        Err(Box::new(StringError::new(output_buf)))
    }
}

#[test]
fn test_gpg() {
    let file: Vec<u8> = (1..500000).map(|_| 1).collect();

    let conf = GpgConfig {
        recipient: "contact@lovisotto.it".to_string(),
    };
    let outcome = encrypt(&conf, file, "prova.pdf");
    assert!(outcome.is_ok());
}

#[test]
fn test_gpg_wrong_address() {
    let file: Vec<u8> = (1..500000).map(|_| 6).collect();

    let conf = GpgConfig {
        recipient: "no_gpg@lovisotto.it".to_string(),
    };
    let outcome = encrypt(&conf, file, "prova.pdf");
    assert!(outcome.is_err());
}
